import * as i2c from "i2c-bus";

const ADDRESS_LCD = 0x3e;

const NEW_LINE = 0xc0;
const CLEAR = 0x01;
const SPACE = 0x20;

const wait = (ms: number) => {
  return new Promise((resolve, reject) => setTimeout(resolve, ms));
};

const writeCommand = (i2c1, command: number) => {
  i2c1.writeByte(ADDRESS_LCD, 0x00, command);
};
const writeData = (i2c1, data: number) => {
  i2c1.writeByte(ADDRESS_LCD, 0x40, data);
};

const readFile = (path: string): Promise<string> => {
  return new Promise((resolve, reject) => {
    const fs = require("fs");

    fs.readFile(path, "utf8", (err, data) => {
      if (err) {
        console.error(err);
        reject(err);
      }
      resolve(data);
    });
  });
};

const init = async (i2c1) => {
  writeCommand(i2c1, 0x38);
  await wait(10);
  writeCommand(i2c1, 0x39);
  await wait(10);
  writeCommand(i2c1, 0x14);
  await wait(10);
  writeCommand(i2c1, 0x70);
  await wait(10);
  writeCommand(i2c1, 0x56);
  await wait(10);
  writeCommand(i2c1, 0x6c);
  await wait(10);
  writeCommand(i2c1, 0x38);
  await wait(10);
  writeCommand(i2c1, 0x0c);
  await wait(10);
  writeCommand(i2c1, 0x01);
  await wait(10);
};

const getTimeTableList = async (path: string) => {
  const timeTable = await readFile(path);
  const timeTableList = timeTable.split("\n");
  timeTableList.sort();
  return timeTableList;
};

const addMinutes = (date, minutes): Date => {
  return new Date(date.getTime() + minutes * 60000);
};

const BUFFER_MINUTE = 4;

(async () => {
  const i2c1 = await i2c.openPromisified(1);

  await init(i2c1);

  await wait(100);
  writeCommand(i2c1, CLEAR);

  while (true) {
    const targetDate = addMinutes(new Date(), BUFFER_MINUTE);
    const targetHour = ("0" + String(targetDate.getHours())).slice(-2);
    const targetMinute = ("0" + String(targetDate.getMinutes())).slice(-2);
    const targetTime = `${targetHour}:${targetMinute}`;

    const osakaWeekendDiffMinute = await getDiffMinute(
      targetTime,
      "./timetable_hankyu_weekend_osaka.txt"
    );
    const kobeWeekendDiffMinute = await getDiffMinute(
      targetTime,
      "./timetable_hankyu_weekend_kobe.txt"
    );
    const osakaWeekdayDiffMinute = await getDiffMinute(
      targetTime,
      "./timetable_hankyu_weekday_osaka.txt"
    );
    const kobeWeekdayDiffMinute = await getDiffMinute(
      targetTime,
      "./timetable_hankyu_weekday_kobe.txt"
    );

    writeTimeTable(i2c1, 0xb5, 0xb7, osakaWeekendDiffMinute);
    writeCommand(i2c1, NEW_LINE);
    writeTimeTable(i2c1, 0xba, 0xb7, kobeWeekendDiffMinute);

    await wait(2000);
    writeCommand(i2c1, CLEAR);
    await wait(2000);


    writeTimeTable(i2c1, 0xb5, 0xcd, osakaWeekdayDiffMinute);
    writeCommand(i2c1, NEW_LINE);
    writeTimeTable(i2c1, 0xba, 0xcd, kobeWeekdayDiffMinute);

    await wait(2000);
    writeCommand(i2c1, CLEAR);

  }
})();

const getDiffMinute = async (targetTime: string, path: string) => {
  const timeTableList = await getTimeTableList(path);
  const nextTimeTable = calcNextTimeTable(targetTime, timeTableList);
  const diffMinute = calcDiffMinute(nextTimeTable);
  return diffMinute;
};

const writeTimeTable = (
  i2c1,
  headChar1: number,
  headChar2: number,
  diffMinute: number
) => {
  writeData(i2c1, headChar1);
  writeData(i2c1, headChar2);
  writeData(i2c1, SPACE);

  writeData(i2c1, 0xb1);
  writeData(i2c1, 0xc4);
  writeData(i2c1, diffMinute > 10 ? 0x30 + Math.floor(diffMinute / 10) : SPACE);
  writeData(i2c1, 0x30 + (diffMinute % 10));
  writeData(i2c1, 0x6d);

  // const length = characters.length
  // const codeList = []
  // for (let index = 0; index < characters.length; index++) {
  //     const code = characters.charCodeAt(index)

  //     const distance = code - 12450
  //     console.log(distance)
  //     const distanceForCode = distance /2

  //     codeList.push(0xb1 + distanceForCode)
  //     console.log((0xb1 + distanceForCode).toString(16))
  // }

  // console.log(codeList)
};

const calcNextTimeTable = (
  targetTime: string,
  timeTableList: string[]
): string => {
  // 次の時刻を算出
  const filtered = timeTableList.filter((t) => t > targetTime);
  return filtered.length !== 0 ? filtered[0] : timeTableList[0];
};

const calcDiffMinute = (time: string) => {
  const y = new Date().getFullYear();
  const m = new Date().getMonth();
  const d = new Date().getDate();
  const nextTimeTableDate = new Date(
    y,
    m,
    d,
    Number(time.split(":")[0]),
    Number(time.split(":")[1])
  );

  return Math.ceil(
    (nextTimeTableDate.getTime() - new Date().getTime()) / 1000 / 60
  );
};
